# Import the database object (db) from the main application module
from datetime import datetime, time, timedelta
import logging
from decimal import Decimal
import os
import base64
import pandas as pd
import onetimepass as otp
import hvac
from sqlalchemy import *
from sqlalchemy.orm import *
from sqlalchemy.orm.collections import attribute_mapped_collection
from sqlalchemy.types import *
from sqlalchemy.dialects.mysql import INTEGER as Integer
from sqlalchemy.ext.hybrid import hybrid_property
from flask import Flask
from sqlalchemy import exc, event, select
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import check_password_hash, generate_password_hash
from flask_user import UserMixin


# Define the WSGI application object
app = Flask(__name__, instance_relative_config=True)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# Configurations
try:
    app.config.from_pyfile('config.py')
except:
    pass

app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv('SQLALCHEMY_DATABASE_URI')

db = SQLAlchemy(app)
Base = db.Model
db_session = db.session
engine = db.engine

# Encryption/Decryption
def vault_encryption_seed():
    print('VAULT URL')
    print(os.getenv('VAULT_URL'))
    print('VAULT TOKEN')
    print(os.getenv('VAULT_TOKEN'))
    client = hvac.Client(url=os.getenv('VAULT_URL'))
    client.token = os.getenv('VAULT_TOKEN').splitlines()[0]
    vault_seed = str(client.read('secret/coincube')['data']['secretKey'])
    print('VAULT SEED')
    print(vault_seed)
    return vault_seed

try:
    VAULT_SEED = vault_encryption_seed()
except Exception as e:
    app.logger.debug('Unable to retrieve Vault seed.')
    app.logger.debug(e)
    VAULT_SEED = 'LS0tLS1CRUdJTiBSU0EgUFJJVkFURSBLRVktLS0tLQpNSUlFb2dJQkFBS0NBUUVBZ1ZLbHRMSzBGNmtuSitsSEUxSWpZLzVtYjlRUnloMzRLeUxuRTFLS2hIcnRZTDJtCndRWEtUdDB0Y2dzZGtteWUzSVlPSlIvQjlOckV6aWFwL0tPaUlmc2JGdGpSTmk1akFUQUpnVW9LU3daVENxaTcKcklOdHRac3VIaGFlY0phWTNyR0M5N3B2cjRZWHQ0UG1QMXZTYnhzS1hXUjNiV3ZmUDRjRk8xSDZ3ZVlEQUljMwo5SVF4MDJFT2c1cWxHT2pnY0M1Z0l3aHBaN1dnMUVQU2dZek83MzRZNEFtZVhNb3VkdHdvNzNncGtiU1luM1BoCjg3VFF5cDl4cXp4NXZtcjJNdWQzbHR3MGR1NlZtWmM4VVhQTnhoMVRkUmMxcWdiM2dqMEpVcTZEQ0RVbEhIWDUKU0ljWkdsd2NjWkRPNzFjRWY4SkhLU25pbXBXcUdpZ2c2eldLMHdJREFRQUJBb0lCQUQzV01BZmtQenJsRmlUKwpaWTcyZ3Blb3FJQ21iYzh3WDE3NDlIY0h5OVluRldSaHF0KzNEaEtCcmQ1UE5GZTBlbzNGeC9PMmFMN1U4TEFHClN0ZlR3bzN2RFBmeXVxN21wVTZIRXJSd205VmQ4ODB2KzNVMDd2dWNkUVBNR3Z5STloWGQyVnJ0R2NSYklQSU8KTHNWeStYOUk5YjdNY0ZLdzl2SGdkUDcxWnlyWlMrb0VMenpVZWhSQ2pZVjhTQ2lWZCt3UzdMRmw3b2ZkOEZyLwpYSVM4SlpMTWN6M1c1SFpVMEYzSWczdUJkaVlRNWV4dUdudzF2Z1Q4NEZDWERpSUZpNWZrSThBUmxTTzkyNnVyCldid1N0bHdockpvTmQ0V0FnZ0xsMWFYa0JDazdEd1RUSDZxbHhJUlEvWFR3MFQwc2VmUEJ5Wmw4YzRHaGdkaU8KcHJGSW45RUNnWUVBdFEzQU5XMjZwNHdXdlFoYTQ0aldKczFKYnpDQlpyWE5WU28yN2hXZnloV3JHZUN0M0ZhTAowTFl6M1lpYUxHUGJ4Z3MzOUEzdGhYWDNtb3BhMXdNNzBTQlNmUVE1MHMzUndXTkZXZ1plOGJqOVVZeGJLSGZsCjNpaUc2NWJTaUpZbExBZlBDVmswODhMckh4Y0dOaTJtRjBpRFdlZjkvUGxTenFxM09tTHU5N3NDZ1lFQXR0cjYKd3BHOGhPT1FJbkZLMWlhQlFFVDNKd0pGeWFyQlptRFU1VjVSa0VyWWZtRGJmNFJDcENaOCtJWkpYRCtqcEF0VgpYbm04KzVmQUprcjdrMGhSb1dGdkRVZFgvWk9OMElnZVV4RUFxV0R1NDAwWDVOREFZeDJTRWFWRUQvZ3pKZE1iCjVwRUxTdVVieFJPRUFLMkQ3Ly9tdGxTVzRiTW83cVlkSVlQUUM4a0NnWUJKYk1Rd1pzbTF2UmhUblJuUEFPVm8KWXdTTzlTcmxUb0Z4TW1lSWNuaVU2ZWl6Mit4bVdTcm1mNTNWRDlzR2VaRGRaaHRRL0gwQkRQQlhXaGl3N2Z6SwpPTU43TThXSGVqVEFxdE9yU0w0a1paTkdDUmZHOTZqZlNhdzl5cktpOWRFcFpCbFFmejc4UkdZNSs0cG5XbWZyCk5Wc2ZrUGZ1S2VhdExZcVZRUjRNbFFLQmdBTjhIdVZRWTdoZ29lR1dLU2YxcWVid3FZa3J5T3F6LzJXVkdObWEKTjVHNWEzRFZKYm5EYW9XNFdnOWVkODF6UlFvZVVTMHd3c0pFdlJ1OXVCZXhmQ1RJTjVhWU1ud1pzTTAxbXVaWQpYNXZheGhLODZHU3ErYytwdzV2a2UyeVdmVnNPTCt6di9MZ2JyN0RNMDJvejVGelJrck1NMzZkRGc5Y3dnVU9mClFIckpBb0dBSy81ZktFcG5MMjEyQ2d5N3dtL2x3K2NvNVV4RENJTCtES1FvV3EzY1NLV1h4TFJLYnVkRVJiTTYKQUNSZ2N1bXRUM3hwYnRTcEZHMjlEQUI4SW5RczhXMUZIdlUyeVRCRlNGMVRMbytGblRVWkFoNnJNSm1aaG8rdgp3RFhjcTVYTXdzLzFSa2ZjbXdTeVZsaEZaQzZUamVaNWQ4cE8wWjBoV2Z4SGViZ1pYRTg9Ci0tLS0tRU5EIFJTQSBQUklWQVRFIEtFWS0tLS0t'

FKInteger = Integer(10, unsigned=True)
VAL_SYM = 'BTC'
DUST_AMT = 9e-8

log = logging.getLogger(__name__)


@event.listens_for(db.engine, "engine_connect")
def ping_connection(connection, branch):
    if branch:
        # "branch" refers to a sub-connection of a connection,
        # we don't want to bother pinging on these.
        return

    # turn off "close with result".  This flag is only used with
    # "connectionless" execution, otherwise will be False in any case
    save_should_close_with_result = connection.should_close_with_result
    connection.should_close_with_result = False

    try:
        # run a SELECT 1.   use a core select() so that
        # the SELECT of a scalar value without a table is
        # appropriately formatted for the backend
        connection.scalar(select([1]))
    except exc.DBAPIError as err:
        # catch SQLAlchemy's DBAPIError, which is a wrapper
        # for the DBAPI's exception.  It includes a .connection_invalidated
        # attribute which specifies if this connection is a "disconnect"
        # condition, which is based on inspection of the original exception
        # by the dialect in use.
        if err.connection_invalidated:
            # run the same SELECT again - the connection will re-validate
            # itself and establish a new connection.  The disconnect detection
            # here also causes the whole connection pool to be invalidated
            # so that all stale connections are discarded.
            connection.scalar(select([1]))
        else:
            raise
    finally:
        # restore "close with result"
        connection.should_close_with_result = save_should_close_with_result

def init_db():
    # import all modules here that might define models so that
    # they will be registered properly on the metadata.  Otherwise
    # you will have to import them first before calling init_db()
    Base.metadata.create_all(bind=engine)


def d(v, seed=VAULT_SEED):
    # for simplicity, returns v on failure
    from Crypto.PublicKey import RSA
    from Crypto.Cipher import PKCS1_OAEP
    from base64 import b64decode
    try:
        b = seed
        k = b64decode(b)
        r = RSA.importKey(k)
        p = PKCS1_OAEP.new(r)
        d = p.decrypt(b64decode(v)).decode('utf-8')
        return d
    except:
        return v

def e(v, seed=VAULT_SEED):
    # for simplicity, returns v on failure
    from Crypto.PublicKey import RSA
    from Crypto.Cipher import PKCS1_OAEP
    from base64 import b64decode, b64encode
    try:
        b = seed
        k = b64decode(b)
        r = RSA.importKey(k)
        p = PKCS1_OAEP.new(r)
        e = p.encrypt(v.encode('utf-8'))
        be = b64encode(e).decode('utf-8')
        return be
    except:
        raise

def get_prices(frequency='H'):
    val_cur = Currency.query.filter_by(symbol=VAL_SYM).one()
    prices = pd.DataFrame()
    for ip in IndexPair.query.filter(or_(
        IndexPair.base_currency == val_cur,
        IndexPair.quote_currency == val_cur
        )):

        try:
            c = ip.candle_1h_query.with_entities(
                    CombinedIndexPair1h.timestamp, 
                    CombinedIndexPair1h.close
                    ).filter(CombinedIndexPair1h.timestamp > '2018-05-01'
                    ).all()
        except NotImplementedError:
            continue
        
        c = pd.DataFrame(c, columns=['timestamp', 'price'])
        c.timestamp = pd.to_datetime(c.timestamp)
        if ip.quote_currency == val_cur:
            c['symbol'] = ip.base_currency.symbol
        else:
            c['symbol'] = ip.quote_currency.symbol
            c.price = 1 / c.price
        prices = prices.append(c, ignore_index=True)
    prices = prices.set_index(['symbol', 'timestamp']).sort_index()
    if frequency == 'D':
        prices = prices.reset_index().set_index('timestamp').groupby(['symbol']).price.resample(frequency).last()
        prices = prices.reset_index().set_index(['symbol', 'timestamp'])
    prices.price = prices.price.astype('float64')
    prices = prices.fillna(method='ffill')
    return prices
    

class Mixin(object):
    id = Column(FKInteger, primary_key=True)
    created_at = Column(DateTime, default=datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)

    def save_to_db(self):
        db_session.add(self)
        db_session.commit()


class CandleMixin(object):
    timestamp = Column(TIMESTAMP, primary_key=True)
    close = Column(Numeric(24,12))
    high = Column(Numeric(24,12))
    low = Column(Numeric(24,12))
    volume = Column(Numeric(26,12))

    def __repr__(self):
        return '{s.timestamp} C: {s.close} H: {s.high} L: {s.low} V: {s.volume}'.format(
            s=self)


class Algorithm(Mixin, Base):
    __tablename__ = 'algorithms'

    name = Column(String(50))
    description = Column(String(150))
    active = Column(Boolean())

    algorithm_features = relationship('AlgorithmFeature')

    @property 
    def features(self):
        af = AlgorithmFeature.query.filter_by(
                    algorithm_id=self.id,
                    ).first()
        return [af.f_one, af.f_two, af.f_three, af.f_four, af.f_five, af.f_six]

    def __repr__(self):
        return '<Algorithm {s.id} ({s.name}): active={s.active}>'.format(s=self)


class AlgorithmFeature(Base):
    __tablename__ = 'algorithm_features'

    id = Column(FKInteger, primary_key=True)
    algorithm_id = Column(FKInteger, ForeignKey('algorithms.id'))
    f_one = Column(String(256))
    f_two = Column(String(256))
    f_three = Column(String(256))
    f_four = Column(String(256))
    f_five = Column(String(256))
    f_six = Column(String(256))

    def __repr__(self):
        return '<Algorithm Feature {s.algorithm_id}>'.format(s=self)


class AlgoAllocation(Mixin, Base):
    __tablename__ = 'algo_allocations'

    cube_id = Column(FKInteger, ForeignKey('cubes.id'))
    algorithm_id = Column(FKInteger, ForeignKey('algorithms.id'))
    percent = Column(Numeric(20,2))

    algorithm = relationship('Algorithm')

    def __repr__(self):
        return '<AlgoAllocation(id={s.id}, cube_id={s.cube_id}, algorithm_id={s.algorithm_id}, '\
             'percent={s.percent})>'.format(s=self)


class AssetAllocation(Mixin, Base):
    __tablename__ = 'allocations'

    cube_id = Column(FKInteger, ForeignKey('cubes.id'))
    currency_id = Column(FKInteger, ForeignKey('currencies.id'))
    percent = Column(Numeric(24,12))
    reserved_base = Column(Numeric(24,12))
    reserved_quote = Column(Numeric(24,12))

    currency = relationship('Currency')

    def __repr__(self):
        return '<AssetAllocation(id={s.id}, cube_id={s.cube_id}, currency_id={s.currency_id}, '\
            'currency={s.currency}, percent={s.percent})>'.format(s=self)

    __table_args__ = (UniqueConstraint('cube_id', 'currency_id'), )


class AumBTC(Base):
    __tablename__ = 'aum_btc_per_ex'

    index = Column(DateTime, primary_key=True)
    Bitfinex = Column(Numeric(24,12))
    Bitstamp = Column(Numeric(24,12))
    Bittrex = Column(Numeric(24,12))
    Coinbase = Column(Numeric(24,12))
    Gemini = Column(Numeric(24,12))
    Kraken = Column(Numeric(24,12))
    Poloniex = Column(Numeric(24,12))
    HitBTC = Column(Numeric(24,12))
    Binance = Column(Numeric(24,12))
    total = Column(Numeric(24,12))

    def __repr__(self):
        return '<AumBTC(id={s.index}, total={s.total})>'.format(s=self)


class BacktestResults(Base):
    __tablename__ = 'backtest_results'

    # hard to store positions or other data; figure out a way to do that
    MAX_NAME_LENGTH = 50

    timestamp = Column(TIMESTAMP, primary_key=True)
    portfolio_value = Column(Float(53))
    backtest_name = Column(String(MAX_NAME_LENGTH), primary_key=True)
    index_name = Column(String(50))

    __table_args__ = (UniqueConstraint('timestamp', 'backtest_name'), )

    def __repr__(self):
        rep = ("Backtest {s.backtest_name} @ {s.timestamp}. Index name: {s.index_name}"
               " Portfolio value: {s.portfolio_value}")
        return rep.format(s=self)


class BacktestResultsSecondary(Base):
    __tablename__ = 'backtest_results_secondary'

    # hard to store positions or other data; figure out a way to do that
    MAX_NAME_LENGTH = 50

    timestamp = Column(TIMESTAMP, primary_key=True)
    portfolio_value = Column(Float(53))
    backtest_name = Column(String(MAX_NAME_LENGTH), primary_key=True)
    index_name = Column(String(50))

    __table_args__ = (UniqueConstraint('timestamp', 'backtest_name'), )

    def __repr__(self):
        rep = ("Backtest {s.backtest_name} @ {s.timestamp}. Index name: {s.index_name}"
               " Portfolio value: {s.portfolio_value}")
        return rep.format(s=self)


class Balance(Mixin, Base):
    __tablename__ = 'balances'

    cube_id = Column(FKInteger, ForeignKey('cubes.id'))
    currency_id = Column(FKInteger, ForeignKey('currencies.id'))
    exchange_id = Column(FKInteger, ForeignKey('exchanges.id'))
    available = Column(Numeric(24,12))
    total = Column(Numeric(24,12))
    last = Column(Numeric(24,12))  # or use tx fk / property...
    target = Column(Numeric(24,12))

    cube = relationship('Cube')
    currency = relationship('Currency')
    exchange = relationship('Exchange')

    @property
    def ex_pair(self):
        ex_pair_query = ExPair.query.filter_by(exchange_id=self.exchange_id)
        if self.currency.symbol == 'BTC':
            ex_pair = ex_pair_query.filter(
                ExPair.quote_currency_id == self.cube.fiat_id,
                ExPair.base_currency_id == self.currency_id
            ).first()
        else:
            ex_pair = ex_pair_query.filter(
                    ExPair.quote_symbol == 'BTC',
                    ExPair.base_currency_id == self.currency_id
                ).first()
            if not ex_pair:
                ex_pair = ex_pair_query.filter(
                        ExPair.quote_currency_id == self.currency_id,
                        ExPair.base_symbol == 'BTC'
                    ).first()
        return ex_pair  

    @property
    def index_pair(self):
        btc = Currency.query.filter_by(symbol="BTC").first()
        index_pair = IndexPair.query.filter(
                IndexPair.quote_currency_id == btc.id,
                IndexPair.base_currency_id == self.currency_id
            ).first()
        if not index_pair:
            index_pair = IndexPair.query.filter(
                    IndexPair.quote_currency_id == self.currency_id,
                    IndexPair.base_currency_id == btc.id
                ).first()  
        return index_pair          

    @property
    def symbol(self):
        ex_pair = ExPair.query.filter_by(
            exchange_id=self.exchange_id
            ).filter(or_(
                ExPair.quote_currency_id == self.currency_id,
                ExPair.base_currency_id == self.currency_id
                )).first()
        # use currency symbol in event that ex_pair is missing
        if not ex_pair:
            cur = Currency.query.filter_by(id=self.currency_id).first()
            return cur.symbol
        if ex_pair.quote_currency_id == self.currency_id:
            return ex_pair.quote_symbol
        return ex_pair.base_symbol

    @property 
    def btc_rate(self):
        if self.currency.symbol == 'BTC':
            return 1
        else:
            return self.ex_pair.get_close()

    def __repr__(self):
        return '<Balance(cube_id={s.cube_id}, currency={s.currency.symbol}, total={s.total})>'.format(s=self)

    __table_args__ = (UniqueConstraint('cube_id', 'currency_id', 'exchange_id'), )


class Candles1hCCompare(CandleMixin, Base):
    """ Table for storing Crypto Compare 1h data.
    """
    __tablename__ = 'ex_pair_1h_ccompare'

    open = Column(Numeric(24,12))

    ex_pair_id = Column(FKInteger, ForeignKey('ex_pairs.id'), primary_key=True)
    ex_pair = relationship('ExPair', foreign_keys=ex_pair_id)

    def __repr__(self):
        r = ('{s.timestamp} {s.ex_pair.exchange.name} {s.ex_pair.base_symbol}/{s.ex_pair.quote_symbol} '
             'O: {s.open} H: {s.high} L: {s.low} C: {s.close}  V: {s.volume}')

        return r.format(s=self)

    __table_args__ = (UniqueConstraint('timestamp', 'ex_pair_id'),
                      Index('ex_pair_id_index', 'ex_pair_id'))


class Candles1hLocal(CandleMixin, Base):
    """ Table storing 1h data from ohlc/candle collector scripts.
    """
    __tablename__ = 'ex_pair_1h_local'

    open = Column(Numeric(24,12))

    ex_pair_id = Column(FKInteger, ForeignKey('ex_pairs.id'), primary_key=True)
    ex_pair = relationship('ExPair', foreign_keys=ex_pair_id)

    def __repr__(self):
        r = ('{s.timestamp} {s.ex_pair.exchange.name} {s.ex_pair.base_symbol}/{s.ex_pair.quote_symbol} '
             'O: {s.open} H: {s.high} L: {s.low} C: {s.close}  V: {s.volume}')

        return r.format(s=self)

    __table_args__ = (UniqueConstraint('timestamp', 'ex_pair_id'), 
                      Index('ex_pair_id_index', 'ex_pair_id'))


class Candles1hFinal(CandleMixin, Base):
    """ Table storing cleaned and merged 1h data.
    """
    __tablename__ = 'ex_pair_1h_final'

    open = Column(Numeric(24,12))

    ex_pair_id = Column(FKInteger, ForeignKey('ex_pairs.id'), primary_key=True)
    ex_pair = relationship('ExPair', foreign_keys=ex_pair_id)

    def __repr__(self):
        r = ('{s.timestamp} {s.ex_pair.exchange.name} {s.ex_pair.base_symbol}/{s.ex_pair.quote_symbol} '
             'O: {s.open} H: {s.high} L: {s.low} C: {s.close}  V: {s.volume}')

        return r.format(s=self)

    __table_args__ = (UniqueConstraint('timestamp', 'ex_pair_id'), )


class CoinMarketCap(Base):
    __tablename__ = 'coin_market_cap'

    id = Column(FKInteger, primary_key=True)
    symbol = Column(String(10))
    market_cap = Column(Numeric(20, 2))
    price = Column(Numeric(24,12))
    volume = Column(Numeric(20, 2))
    timestamp = Column(DateTime)

    __table_args__ = (UniqueConstraint('timestamp', 'symbol'), )


class CombinedExPair1h(CandleMixin, Base):
    __tablename__ = 'ex_pair_1h'

    open = Column(Numeric(24,12))

    ex_pair_id = Column(FKInteger, ForeignKey('ex_pairs.id'), primary_key=True)
    ex_pair = relationship('ExPair', foreign_keys=ex_pair_id)

    def __repr__(self):
        r = ('{s.timestamp} {s.ex_pair.exchange.name} {s.ex_pair.base_symbol}/{s.ex_pair.quote_symbol} '
             'O: {s.open} H: {s.high} L: {s.low} C: {s.close}  V: {s.volume}')

        return r.format(s=self)

    __table_args__ = (UniqueConstraint('timestamp', 'ex_pair_id'), 
                      Index('ex_pair_id_index', 'ex_pair_id'))


class CombinedIndexPair1h(CandleMixin, Base):
    __tablename__ = 'index_pair_1h'

    open = Column(Numeric(24,12))

    quote_currency_id = Column(FKInteger, ForeignKey('currencies.id'), primary_key=True)
    base_currency_id = Column(FKInteger, ForeignKey('currencies.id'), primary_key=True)
    index_pair_id = Column(FKInteger, ForeignKey('index_pairs.id'), primary_key=True)
    index_pair = relationship('IndexPair', foreign_keys=index_pair_id)
    candle_number = Column(Integer(2, unsigned=True))

    base = relationship('Currency', foreign_keys=base_currency_id)
    quote = relationship('Currency', foreign_keys=quote_currency_id)

    def __repr__(self):
        r = ('IndexPair: {s.timestamp} {s.base.symbol}/{s.quote.symbol} '
             'C: {s.close} H: {s.high} L: {s.low} V: {s.volume}')

        return r.format(s=self)

    __table_args__ = (UniqueConstraint('timestamp', 'index_pair_id'),
                      Index('base_quote_index', 'quote_currency_id', 'base_currency_id'))


class Connection(Mixin, Base):
    __tablename__ = 'api_connections'

    user_id = Column(FKInteger, ForeignKey('users.id'))
    cube_id = Column(FKInteger, ForeignKey('cubes.id'))
    exchange_id = Column(FKInteger, ForeignKey('exchanges.id'))
    key = Column(String(400))
    secret = Column(String(400))
    passphrase = Column(Text)
    failed_at = Column(DateTime)
    liquidation_currency_id = Column(FKInteger, ForeignKey('currencies.id'),
        nullable=True)

    exchange = relationship('Exchange')
    liquidation_currency = relationship('Currency')

    @property 
    def decrypted_key(self):
        return d(self.key)

    @property 
    def decrypted_secret(self):
        return d(self.secret)

    @property 
    def decrypted_passphrase(self):
        return d(self.passphrase)

    def api(self, exapi):
        k = d(self.key)
        s = d(self.secret)
        p = d(self.passphrase)
        return exapi.exs[self.exchange.name](k, s, p)

    def __repr__(self):
        return '<Connection(id={s.id}, cube_id={s.cube_id}, exchange_id={s.exchange_id}, '\
            'exchange={s.exchange}, failed_at={s.failed_at})>'.format(s=self)


class ConnectionError(Mixin, Base):
    __tablename__ = 'api_connection_errors'

    user_id = Column(FKInteger, ForeignKey('users.id'))
    cube_id = Column(FKInteger, ForeignKey('cubes.id'))
    exchange_id = Column(FKInteger, ForeignKey('exchanges.id'))
    error_message = Column(String(400))

    exchange = relationship('Exchange')

    def __repr__(self):
        return '<ConnectionError(id={s.id}, cube_id={s.cube_id}, exchange_id={s.exchange_id}, '\
            'exchange={s.exchange}, error_message={s.error_message})>'.format(s=self)


class Cube(Mixin, Base):
    __tablename__ = 'cubes'

    user_id = Column(FKInteger, ForeignKey('users.id'))
    algorithm_id = Column(FKInteger, ForeignKey('algorithms.id'))
    trading_status = Column(Enum('live', 'off'), default='live')
    closed_at = Column(DateTime, nullable=True)
    suspended_at = Column(DateTime, nullable=True)
    fiat_id = Column(FKInteger, ForeignKey('currencies.id'))
    threshold = Column(Numeric(20,2), nullable=True)
    rebalance_interval = Column(Integer(10), nullable=True)
    balanced_at = Column(DateTime, nullable=True)
    reallocated_at = Column(DateTime, nullable=True)
    risk_tolerance = Column(Integer(2))
    focus_id = Column(FKInteger, ForeignKey('focuses.id'), nullable=True)
    auto_rebalance = Column(Boolean, nullable=True)
    unrecognized_activity = Column(Boolean, nullable=True)
    requires_exchange_transfer = Column(Boolean, default=False)
    name = Column(String(20))
    exchange_id = Column(FKInteger, ForeignKey('exchanges.id'))
    update_charts = Column(Boolean, default=False)

    user = relationship('User')
    algorithm = relationship('Algorithm')
    fiat = relationship('Currency')
    cube_cache = relationship('CubeCache')
    dollar_cost_average = relationship('DollarCostAverage')
    exchange = relationship('Exchange')

    api_connections = relationship('Connection')
    connections = relationship('Connection',
        collection_class=attribute_mapped_collection('exchange.name'),
        backref='cube')
    algo_allocations = relationship('AlgoAllocation',
        collection_class=attribute_mapped_collection('algorithm_id'),
        backref='cube')
    allocations = relationship('AssetAllocation',
        collection_class=attribute_mapped_collection('currency.symbol'),
        backref='cube')
    balances = relationship('Balance')
    custom_portfolios = relationship('CustomPortfolio',
        backref='cube')
    daily_performance = relationship('DailyPerformance',
        lazy='dynamic',
        backref='cube')
    external_addresses = relationship('ExternalAddress',
        backref='cube')
    external_transactions = relationship('ExternalTransaction',
        backref='cube')
    focus = relationship('Focus')
    hourly_performance = relationship('HourlyPerformance',
        lazy='dynamic',
        backref='cube')
    orders = relationship('Order')
    all_orders = relationship('Order',
        collection_class=attribute_mapped_collection('order_id'),
        backref='cube',
        cascade='all, delete, delete-orphan')
    transactions = relationship('Transaction',
        lazy='dynamic',
        backref='cube')
    transactions_full = relationship('TransactionFull',
        lazy='dynamic')

    @property
    def val_cur(self):
        ### TO DO
        ## Should be a Cube attribute (i.e. cube.var_cur) which user can select
        return Currency.query.filter_by(symbol='BTC').first()

    @property 
    def is_rebalancing(self):
        if self.reallocated_at and self.balanced_at:
            if self.reallocated_at > self.balanced_at:
                return True
            else:
                return False
        if self.reallocated_at:
            return True
        else:
            return False

    @property
    def supported_assets(self):
        # Check to see if assets are available on connected exchanges
        supported_assets = []
        for conn in self.connections.values():
            ex_pairs = ExPair.query.filter_by(
                exchange_id=conn.exchange.id,
                active=True
            )

            for pair in ex_pairs:
                if pair.quote_currency not in supported_assets:
                    supported_assets.append(pair.quote_currency)
                if pair.base_currency not in supported_assets:
                    supported_assets.append(pair.base_currency)

        # Order by market capitalization (highest to lowest)
        ordered_assets = []
        for cur in supported_assets:
            ordered_assets.append((cur.market_cap, cur.symbol))
        ordered_assets.sort(key=lambda x: x[0] or 0, reverse=True)

        sorted_assets = []
        for x in ordered_assets:
            sorted_assets.append(x[1])

        return sorted_assets

    def get_external_balances(self):
        accounts = list(map(lambda ex: ex.balances, self.external_addresses))
        flattened_accounts = [balance for account in accounts for balance in account]
        return flattened_accounts

    def log_user_action(self, action_name, details=None):
        a = CubeUserAction(
            cube_id = self.id,
            action = action_name,
            details = details)

        db_session.add(a)
        db_session.commit()

    def data_frame(self, query, columns):
        # Takes a sqlalchemy query and a list of columns, returns a dataframe.
        def make_row(x):
            return dict([(c, getattr(x, c)) for c in columns])
        return pd.DataFrame([make_row(x) for x in query])

    def get_val_btc(self, bal, ex_id, cur_id):
        if cur_id == 2: # If BTC, return balance
            return bal
        else:
            try:
                if ex_id == 12: # External
                    ex_pair = IndexPair.query.filter_by(
                        exchange_id=ex_id,
                        quote_currency_id=2,
                        base_currency_id=cur_id).first()
                    if ex_pair == None:
                        ex_pair = IndexPair.query.filter_by(
                            exchange_id=ex_id,
                            quote_currency_id=cur_id,
                            base_currency_id=2).first()
                else:
                    # Find BTC ex_pair for incoming cur_id
                    ex_pair = ExPair.query.filter_by(
                        exchange_id=ex_id,
                        quote_currency_id=2,
                        base_currency_id=cur_id).first()
                    if ex_pair == None:
                        ex_pair = ExPair.query.filter_by(
                            exchange_id=ex_id,
                            quote_currency_id=cur_id,
                            base_currency_id=2).first()

                close = ex_pair.get_close()

                if ex_pair.quote_currency_id in [1,3,4,5,6]: # Fiat currencies or XBT/LTC
                    val_btc = bal / Decimal(close)
                else:
                    val_btc = bal * Decimal(close)
            except:
                val_btc = 0

            return val_btc

    def get_ex_bals(self):
        balances = {}
        for bal in self.balances:
            if not bal.exchange.name in balances:
                balances[bal.exchange.name] = [bal]
            else:
                balances[bal.exchange.name].append(bal)
        return balances        

    def valuations(self):
        # sets val_btc and val_fiat for individual balances, and
        # returns dict of total btc and fiat valuations
        btc = Currency.query.filter_by(symbol='BTC').one()
        ep = IndexPair.query.filter_by(quote_currency=self.user.fiat, base_currency=btc).first()
        log.debug(ep)
        btc_price = float(ep.get_close())
        log.debug('BTC price', btc_price)

        val_btc = 0
        for b in self.balances:
            log.debug(b)
            if b.currency == btc:
                b.val_btc = float(b.total)
            elif not b.total:
                b.val_btc = 0
            else:
                q = IndexPair.query.filter_by(active=True)
                ep = q.filter_by(quote_currency=btc, base_currency=b.currency).first()
                log.debug(ep)
                if not ep:
                    ep = q.filter_by(base_currency=btc, quote_currency=b.currency).first()
                    flipped = True
                else:
                    flipped = False
                # Use ExPair if no IndexPair available
                if not ep:
                    q = ExPair.query.filter_by(active=True)
                    ep = q.filter_by(quote_currency=btc, base_currency=b.currency).first()
                    if not ep:
                        ep = q.filter_by(base_currency=btc, quote_currency=b.currency).first()
                        flipped = True
                    else:
                        flipped = False
                log.debug(ep)
                price = float(ep.get_close())
                log.debug(price)
                if flipped:
                    price = 1 / price
                b.val_btc = float(b.total) * price
            log.debug(b.val_btc)
            val_btc += b.val_btc
            # b.val_fiat = b.val_btc * btc_price
        val_fiat = val_btc * btc_price

        return {'val_btc': val_btc, 'val_fiat': val_fiat}

    def tx_to_ledger(self, transactions=None,
                    start_date=None, end_date=None):
        if transactions is None:
            transactions = self.transactions
        if start_date:
            transactions = transactions.filter(
                Transaction.created_at >= start_date)
        if end_date:
            transactions = transactions.filter(
                Transaction.created_at <= end_date)
        txs = [(tx.created_at, tx.type, tx.ex_pair.exchange.name,
            tx.ex_pair.quote_currency.symbol, tx.quote_amount,
            tx.ex_pair.base_currency.symbol, tx.base_amount)
            for tx in transactions]

        ledger = []
        for tx in txs:
            if tx[4]:
                ledger.append((tx[0], tx[1], tx[2], tx[3], float(tx[4])))
            if tx[6]:
                ledger.append((tx[0], tx[1], tx[2], tx[5], float(tx[6])))
        return ledger

    def get_performance(self, start_date=None, end_date=None, timeframe='hourly'):
        btc = Currency.query.filter_by(symbol='BTC').one()

        try:
            # Fiat total and percent return
            index_pair = IndexPair.query.filter_by(
                quote_currency_id=self.user.fiat.id,
                base_currency_id=btc.id
                ).first()

            if timeframe == 'daily':
                dp = self.get_daily_performance(start_date, end_date)
                # Import data from
                query = index_pair.candle_1h_query.filter(
                    func.time(CombinedIndexPair1h.timestamp) == time(23),
                    CombinedIndexPair1h.timestamp >= dp.index[0],
                    CombinedIndexPair1h.timestamp <= dp.index[-1]
                    ).all()
                # get latest candle for end day
                last = index_pair.candle_1h_query.filter(
                    CombinedIndexPair1h.timestamp <= dp.index[-1]
                    ).order_by(CombinedIndexPair1h.timestamp.desc()).first()
                if last:
                    query.append(last)
            if timeframe == 'hourly':
                dp = self.get_hourly_performance(start_date, end_date)
                query = index_pair.candle_1h_query

            # Multiply btc total by fiat.id currency for fiat total
            df2 = self.data_frame(query, ['timestamp', 'close'])
            df2 = df2.set_index('timestamp')

            if timeframe == 'daily':
                df2 = df2.resample('24H').first()
                df2 = df2.reindex(pd.date_range(dp.index[0], dp.index[-1]))
                df2 = df2.ffill()

            df2 = df2[~df2.index.duplicated()]
            dp = dp[~dp.index.duplicated()]

            dp['fiat_total'] = dp.btc_total * df2.close.astype('float64')

            dp['fiat_val_start'] = dp.start_val * df2.close.astype('float64').shift().fillna(df2.close.astype('float64'))
            dp['fiat_val_end'] = dp.end_val * df2.close.astype('float64')

            # Fiat Percentage returns
            dp['fiat_pct_daily'] = (dp.fiat_val_end - dp.fiat_val_start) / dp.fiat_val_start
            dp['fiat_pct'] = dp.fiat_pct_daily.cumsum()
            dp.fiat_pct = dp.fiat_pct * 100

            dp['btc_pct_daily'] = (dp.end_val - dp.start_val) / dp.start_val
            dp['btc_pct'] = dp.btc_pct_daily.cumsum()
            dp.btc_pct = dp.btc_pct * 100

            return dp
        except:
            return pd.DataFrame()
        
    def get_trades(self):
        return self.transactions.filter(Transaction.type.in_(["buy", "sell"])).all()

    def get_daily_performance(self, start_date=None, end_date=None):
        try:
            if start_date and end_date:
                query = self.daily_performance.filter(
                                        DailyPerformance.timestamp >= start_date,
                                        DailyPerformance.timestamp <= end_date
                                        )
            elif start_date:
                query = self.daily_performance.filter(DailyPerformance.timestamp >= start_date)   
            elif end_date:
                query = self.daily_performance.filter(DailyPerformance.timestamp <= end_date)                   
            else:
                query = self.daily_performance
            dp = pd.read_sql(query.statement, query.session.bind)
            dp = dp.set_index('timestamp')
            return dp
        except:
            return pd.DataFrame()

    def get_hourly_performance(self, start_date=None, end_date=None):
        try:
            if start_date and end_date:
                query = self.hourly_performance.filter(
                                        HourlyPerformance.timestamp >= start_date,
                                        HourlyPerformance.timestamp <= end_date
                                        )
            elif start_date:
                query = self.hourly_performance.filter(HourlyPerformance.timestamp >= start_date)   
            elif end_date:
                query = self.hourly_performance.filter(HourlyPerformance.timestamp <= end_date)                   
            else:
                query = self.hourly_performance
            dp = pd.read_sql(query.statement, query.session.bind)
            dp = dp.set_index('timestamp')
            return dp
        except:
            return pd.DataFrame()

    # def get_ledger(self, start_date=None, frequency='H'):
    #     ledger = []
    #     txs = self.transactions
    #     if start_date:
    #         txs = txs.filter(Transaction.created_at >= start_date)
    #         initial = []
    #     for tx in txs:
    #         # Skip inactive assets
    #         if not tx.ex_pair.active:
    #             continue
    #         if start_date:
    #             if (tx.ex_pair.exchange.name,
    #                 tx.ex_pair.base_currency.symbol) not in initial:
    #                 ledger.append((start_date, 'convert',
    #                     tx.ex_pair.exchange.name, 
    #                     tx.ex_pair.base_currency.symbol,
    #                     float(tx.base_balance) - float(tx.base_amount)))
    #                 initial.append((tx.ex_pair.exchange.name, 
    #                     tx.ex_pair.base_currency.symbol))
    #             if (tx.ex_pair.exchange.name,
    #                 tx.ex_pair.quote_currency.symbol) not in initial:
    #                 ledger.append((start_date, 'convert', 
    #                     tx.ex_pair.exchange.name, 
    #                     tx.ex_pair.quote_currency.symbol,
    #                     float(tx.quote_balance) - float(tx.quote_amount)))
    #                 initial.append((tx.ex_pair.exchange.name, 
    #                     tx.ex_pair.quote_currency.symbol))
    #         if tx.base_amount:
    #             ledger.append((tx.created_at, tx.type, tx.ex_pair.exchange.name,
    #                 tx.ex_pair.base_currency.symbol, float(tx.base_amount)))
    #         if tx.quote_amount:
    #             ledger.append((tx.created_at, tx.type, tx.ex_pair.exchange.name,
    #                 tx.ex_pair.quote_currency.symbol, float(tx.quote_amount)))

    #     ledger = pd.DataFrame(ledger,
    #         columns=['timestamp', 'type', 'exchange', 'symbol', 'change'])

    #     if ledger.empty:
    #         dl = ledger[['exchange', 'symbol']]
    #     else:
    #         dl = ledger.groupby(
    #             [pd.Grouper(key='timestamp', freq=frequency, closed='left', label='left'),
    #             'exchange', 'symbol']).sum()
    #         dl['convert'] = ledger[~ledger.type.isin(['buy', 'sell'])].groupby(
    #             [pd.Grouper(key='timestamp', freq=frequency, closed='left', label='left'),
    #             'exchange', 'symbol']).sum().change

    #         dl = dl.reset_index().set_index('timestamp').groupby(
    #             ['exchange', 'symbol']).resample(frequency).sum()
    #         dl = dl.fillna(0)

    #         dl['bal'] = dl.groupby(level=['exchange', 'symbol']).change.cumsum()
    #         dl['bal_end'] = dl.bal
    #         dl['bal_start'] = dl.groupby(
    #             level=['exchange', 'symbol']).bal.shift().fillna(0)

    #         dl = dl.sort_index()
    #         try:
    #             dl.loc[dl.convert < 0, 'bal_end'] = dl.bal_end - dl.convert
    #         except ValueError:
    #             pass
    #         try:
    #             dl.loc[dl.convert > 0, 'bal_start'] = dl.bal_start + dl.convert
    #         except ValueError:
    #             pass

    #     if start_date:
    #         dl = dl.reset_index().set_index(['exchange', 'symbol'])
    #         missing_dl = pd.DataFrame()
    #         for b in self.balances:
    #             ex, sym = b.exchange.name, b.currency.symbol
    #             if (ex, sym) in dl.index:
    #                 continue
    #             elif b.last > DUST_AMT:
    #                 bal = float(b.last)
    #                 missing_dl = missing_dl.append(pd.Series({
    #                     'exchange': ex,
    #                     'symbol': sym,
    #                     'timestamp': start_date,
    #                     'bal': bal,
    #                     'bal_start': bal,
    #                     'bal_end': bal,
    #                     'change': 0,
    #                     'convert': 0
    #                     }), ignore_index=True)
    #             log.debug('Missing ledger entry...')
    #             log.debug(missing_dl)
    #         if not missing_dl.empty:
    #             dl = dl.append(missing_dl.set_index(['exchange', 'symbol']))
    #             dl = dl.reset_index().set_index(
    #                 ['timestamp', 'exchange', 'symbol']).sort_index().reset_index()
    #             start = dl.groupby(['exchange', 'symbol']).first()
    #             start = start[start.timestamp > start_date]
    #             start.timestamp = start_date
    #             start.bal = start.bal - start.change
    #             start.bal_start = start.bal
    #             start.bal_end = start.bal
    #             dl = dl.set_index(['exchange', 'symbol']).append(start)

    #     if dl.empty:
    #         return

    #     del dl['change']
    #     del dl['convert']

    #     dl = dl.reset_index().set_index(
    #         ['timestamp', 'exchange', 'symbol']).sort_index().reset_index()

    #     end = dl.groupby(['exchange', 'symbol']).last()
    #     end_date = pd.to_datetime(datetime.utcnow().date())
    #     end = end[end.timestamp < end_date]
    #     end.timestamp = end_date
    #     end.bal_start = end.bal
    #     end.bal_end = end.bal
    #     dl = dl.set_index(['exchange', 'symbol']).append(end)

    #     dl = dl.reset_index().set_index(
    #         ['timestamp', 'exchange', 'symbol']).sort_index()

    #     dl = dl.reset_index().set_index('timestamp').groupby(
    #         ['exchange', 'symbol']).resample(frequency).first()[
    #         ['bal', 'bal_start', 'bal_end']]
    #     dl.bal = dl.bal.ffill()
    #     dl.bal_start = dl.bal_start.fillna(dl.bal)
    #     dl.bal_end = dl.bal_end.fillna(dl.bal)

    #     dl = dl.reset_index().set_index(
    #         ['timestamp', 'exchange', 'symbol']).sort_index()
    #     return dl


    def get_ledger(self, start_date=None, frequency='D'):
        ledger = []
        txs = self.transactions_full.order_by(TransactionFull.datetime).all()
        print(txs)

        for tx in txs:

            if tx.base_amount:
                ledger.append((tx.datetime, tx.type, tx.exchange.name,
                    tx.base_symbol, float(tx.base_amount)))
            if tx.quote_amount:
                ledger.append((tx.datetime, tx.type, tx.exchange.name,
                    tx.quote_symbol, float(tx.quote_amount)))

        ledger = pd.DataFrame(ledger,
            columns=['datetime', 'type', 'exchange', 'symbol', 'change'])


        if ledger.empty:
            dl = ledger[['exchange', 'symbol']]
        else:
            dl = ledger.groupby(
                [pd.Grouper(key='datetime', freq=frequency, closed='left', label='left'),
                'exchange', 'symbol']).sum()

            dl = dl.reset_index().set_index('datetime').groupby(
                ['exchange', 'symbol']).resample(frequency).sum()
            dl = dl.fillna(0)

            dl['bal'] = dl.groupby(level=['exchange', 'symbol']).change.cumsum()
            dl['bal_end'] = dl.bal
            dl['bal_start'] = dl.groupby(
                level=['exchange', 'symbol']).bal.shift().fillna(0)

            dl = dl.sort_index()

        if dl.empty:
            return

        del dl['change']

        dl = dl.reset_index().set_index(
            ['datetime', 'exchange', 'symbol']).sort_index().reset_index()

        end = dl.groupby(['exchange', 'symbol']).last()
        end_date = pd.to_datetime(datetime.utcnow().date())
        end = end[end.datetime < end_date]
        end.datetime = end_date
        end.bal_start = end.bal
        end.bal_end = end.bal
        dl = dl.set_index(['exchange', 'symbol']).append(end)

        dl = dl.reset_index().set_index(
            ['datetime', 'exchange', 'symbol']).sort_index()

        dl = dl.reset_index().set_index('datetime').groupby(
            ['exchange', 'symbol']).resample(frequency).first()[
            ['bal', 'bal_start', 'bal_end']]
        dl.bal = dl.bal.ffill()
        dl.bal_start = dl.bal_start.fillna(dl.bal)
        dl.bal_end = dl.bal_end.fillna(dl.bal)

        dl = dl.reset_index().set_index(
            ['datetime', 'exchange', 'symbol']).sort_index()
        return dl


    def calc_performance(self, prices, start_date=None, frequency='D'):
        dl = self.get_ledger(start_date, frequency)

        if dl is None:
            return
        syms = dl.groupby(level=['symbol']).groups.keys()
        val_cur = Currency.query.filter_by(symbol='BTC').one()
        dl = dl.reset_index().set_index('datetime')
        for sym in syms:
            index_pair = IndexPair.query.filter(or_(
                            IndexPair.base_symbol == sym,
                            IndexPair.quote_symbol == sym
                            ),
                            IndexPair.active == True
                            ).first()
            if not index_pair:
                dl.loc[(dl.symbol == sym), 'price_end'] = 0
                dl.loc[(dl.symbol == sym), 'price_start'] = 0
            elif sym == VAL_SYM:
                dl.loc[(dl.symbol == sym), 'price_end'] = 1
                dl.loc[(dl.symbol == sym), 'price_start'] = 1
            else:
                if sym not in prices.index:
                    return
                dl.loc[(dl.symbol == sym), 'price_end'] = prices.loc[sym].price
                dl.loc[(dl.symbol == sym), 'price_start'] = prices.loc[sym].price.shift()                

        dl = dl.reset_index().set_index(['datetime', 'symbol'])

        dl['val'] = dl.bal * dl.price_end
        dl['val_end'] = dl.bal_end * dl.price_end
        dl['val_start'] = dl.bal_start * dl.price_start

        dp = dl.groupby(level='datetime').sum()[['val', 'val_end', 'val_start']]

        return dp


    def performance_gen(self, prices, timeframe='hourly'):
        log.debug('[self %d] Start DP' % self.id)

        if timeframe == 'daily':
            last = self.daily_performance.order_by(DailyPerformance.timestamp.desc()).first()
            frequency = 'D'
            log.debug(f'Last from daily_performance: {last}')
        if timeframe == 'hourly':
            last = self.hourly_performance.order_by(HourlyPerformance.timestamp.desc()).first()
            frequency = 'H'
            log.debug(f'Last from hourly_performance: {last}')
        if last:
            start_date = pd.to_datetime(last.timestamp.date()) + timedelta(hours=24)
            log.debug(f'{self} Starting from {start_date}')
        else:
            start_date = None
            log.debug(f'{self} Starting from beginning')

        dp = self.calc_performance(prices, start_date, frequency=frequency)
        if dp is None:
            log.debug(f'{self} No data')
            return
        dp = dp.round(8)
        dp = dp.rename(columns={
            'val': 'btc_total',
            'val_start': 'start_val',
            'val_end': 'end_val'
            })

        dp.index.names = ['timestamp']

        dp['cube_id'] = self.id
        print(dp)

        # Remove last row
        dp = dp[:-1]

        if timeframe == 'daily':
            dp.to_sql(DailyPerformance.__table__.name, db_session.bind, index=True, if_exists='append')
        if timeframe == 'hourly':
            dp.to_sql(HourlyPerformance.__table__.name, db_session.bind, index=True, if_exists='append')            

    def __repr__(self):
        return '[Cube %d]' % self.id


class CubeCache(Base):
    __tablename__ = 'cube_cache'

    cube_id = Column(FKInteger, ForeignKey('cubes.id'), primary_key=True)
    created_at = Column(DateTime, default=datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)
    processing = Column(Boolean)


class CubeUserAction(Mixin, Base):
    __tablename__ = 'cube_user_actions'

    cube_id = Column(FKInteger, ForeignKey('cubes.id'))
    action = Column(String(100))
    details =  Column(Text(), nullable=True)

    cube = relationship('Cube')

    def __repr__(self):
        return '<CubeUserAction {s.id} (cube_id={s.cube_id} action={s.action})>'.format(s=self)


class Currency(Mixin, Base):
    __tablename__ = 'currencies'

    symbol = Column(String(10))
    name = Column(String(50))
    cmc_id = Column(String(50))
    base_protocol = Column(String(10))
    external_blockchain = Column(Boolean)
    fiat = Column(Boolean, default=False)
    num_market_pairs = Column(Integer(10))
    circulating_supply = Column(Integer(15))
    total_supply = Column(Integer(15))
    max_supply = Column(Integer(15))
    price = Column(Numeric(18,6))
    volume_24h = Column(Integer(15))
    percent_change_1h = Column(Numeric(11,8))
    percent_change_24h = Column(Numeric(11,8))
    percent_change_7d = Column(Numeric(11,8))
    market_cap = Column(BigInteger())
    algorithm = Column(String(10))
    proof_type = Column(String(10))
    fully_premined = Column(Boolean, default=False)

    def __repr__(self):
        return '{s.symbol}[{s.id}]'.format(s=self)


class CustomPortfolioCurrency(Base):
    __tablename__ = 'custom_portfolio_currencies'

    id = Column(FKInteger, primary_key=True)
    custom_id = Column(FKInteger, ForeignKey('custom_portfolios.id'))
    currency_id = Column(FKInteger, ForeignKey('currencies.id'))

    def __repr__(self):
        return '%s' % self.currency_id

class CustomPortfolio(Mixin, Base):
    __tablename__ = 'custom_portfolios'

    cube_id = Column(FKInteger, ForeignKey('cubes.id'))
    name = Column(String(255))
    selected = Column(Boolean, default=False)

    custom_currencies = relationship('CustomPortfolioCurrency',
        backref='custom',
        cascade='all, delete-orphan')

    currencies = relationship('Currency',
        secondary='custom_portfolio_currencies',
        order_by="Currency.market_cap")

    def __repr__(self):
        return '%s [%d]' % (self.name, self.id)

class DailyPerformance(Base):
    __tablename__ = 'daily_performance'

    id = Column(FKInteger, primary_key=True)
    cube_id = Column(FKInteger, ForeignKey('cubes.id'))
    timestamp = Column(DateTime)
    btc_total = Column(Numeric(24,12))
    start_val = Column(Numeric(24,12))
    end_val = Column(Numeric(24,12))

    @property
    def daily_return(self):
        if self.start_val == 0:
            return 0
        else:
            return (self.end_val - self.start_val) / self.start_val

    __table_args__ = (UniqueConstraint('cube_id', 'timestamp'), )


class DailyPerformanceAccount(Base):
    __tablename__ = 'daily_performance_account'

    id = Column(FKInteger, primary_key=True)
    user_id = Column(FKInteger, ForeignKey('users.id'))
    timestamp = Column(DateTime)
    start_val = Column(Numeric(24,12))
    end_val = Column(Numeric(24,12))

    @property
    def daily_return(self):
        if self.start_val == 0:
            return 0
        else:
            return (self.end_val - self.start_val) / self.start_val

    __table_args__ = (UniqueConstraint('user_id', 'timestamp'), )


class Discount(Base):
    __tablename__ = 'discounts'

    id = Column(FKInteger, primary_key=True)
    valid_until = Column(DateTime)
    discount_code = Column(String(50))
    start_val = Column(Numeric(4,2))
    product = Column(String(25))


class DollarCostAverage(Base):
    __tablename__ = 'dollar_cost_average'

    id = Column(FKInteger, primary_key=True)
    cube_id = Column(FKInteger, ForeignKey('cubes.id'))
    base_currency_id = Column(FKInteger, ForeignKey('currencies.id'))
    quote_currency_id = Column(FKInteger, ForeignKey('currencies.id'))
    side = Column(Enum('buy', 'sell'))
    value = Column(Numeric(24,12))
    frequency = Column(Integer(10))

    base_currency = relationship('Currency',
        foreign_keys=base_currency_id)
    quote_currency = relationship('Currency',
        foreign_keys=quote_currency_id)

    def __repr__(self):
        return '{s.cube_d} {s.base_currency.symbol}/{s.quote_currency.symbol} [{s.side}] {s.amount}@frequency={s.frequency}'.format(s=self)


class Exchange(Mixin, Base):
    __tablename__ = 'exchanges'

    name = Column(String(50))
    active = Column(Boolean)
    key = Column('label1', String(100))
    secret = Column('label2', String(100))
    passphrase = Column('label3', String(100))
    video_url = Column(String(255))
    signup_url = Column(String(100))

    @property 
    def assets(self):
        ex_pairs = ExPair.query.filter_by(
                        exchange_id=self.id, 
                        active=True
                        ).all()
        assets = []
        for ex_pair in ex_pairs:
            if ex_pair.base_currency not in assets:
                assets.append(ex_pair.base_currency)
            if ex_pair.quote_currency not in assets:
                assets.append(ex_pair.quote_currency)
        assets.sort(key=lambda x: x.symbol, reverse=False)
        return assets

    def public_api(self, exapi):
        return exapi.exs[self.name]

    def api(self, k, s, p, exapi):
        return exapi.exs[self.name](k, s, p)

    def __repr__(self):
        return '{s.name}[{s.id}]'.format(s=self)


class ExPair(Mixin, Base):
    __tablename__ = 'ex_pairs'

    exchange_id = Column(FKInteger, ForeignKey('exchanges.id'))
    quote_currency_id = Column(FKInteger, ForeignKey('currencies.id'))
    base_currency_id = Column(FKInteger, ForeignKey('currencies.id'))
    quote_symbol = Column(String(10))
    base_symbol = Column(String(10))
    active = Column(Boolean)

    candle_1h = Column(Boolean, default=False)

    ex_pair_limits = relationship('ExPairLimits',
        backref='ex_pair_limits')
    ex_pair_close = relationship('ExPairClose')
    exchange = relationship('Exchange',
        backref='ex_pairs')
    quote_currency = relationship('Currency',
        backref='ex_pairs',
        foreign_keys=quote_currency_id)
    base_currency = relationship('Currency',
        foreign_keys=base_currency_id)

    @property
    def candle_1h_query(self):
        if self.exchange.name in ['External', 'Manual']:
            epc = CombinedIndexPair1h.query.filter_by(
                                base_currency_id=self.base_currency_id,
                                quote_currency_id=self.quote_currency_id)
        else:
            epc = CombinedExPair1h.query.filter_by(ex_pair_id=self.id)
        return epc

    @property
    def candle_1h_data(self):
        return self.candle_1h_query.all()

    @property
    def candle_1h_last(self):
        start_timestamp = datetime.utcnow() - timedelta(hours=24)
        if self.exchange.name in ['External', 'Manual']:
            ep_table = CombinedIndexPair1h
            candle = self.candle_1h_query.with_entities(
                    ep_table.timestamp, 
                    ep_table.close
                    ).filter(ep_table.timestamp > start_timestamp
                    ).order_by(ep_table.timestamp.desc()
                    ).first()
        else:
            ep_table = CombinedExPair1h
            candle = self.candle_1h_query.with_entities(
                    ep_table.timestamp, 
                    ep_table.close
                    ).filter(ep_table.timestamp > start_timestamp
                    ).order_by(ep_table.timestamp.desc()
                    ).first()
        if candle:
            return candle.close
        else:
            return 0
 
    def candle_1h_x_days(self, days):
        start_timestamp = datetime.utcnow() - timedelta(days=days)
        if self.exchange.name in ['External', 'Manual']:
            ep_table = CombinedIndexPair1h
            candle = self.candle_1h_query.with_entities(
                    ep_table.timestamp, 
                    ep_table.close
                    ).filter(ep_table.timestamp > start_timestamp
                    ).order_by(ep_table.timestamp
                    ).first()
        else:
            ep_table = CombinedExPair1h
            candle = self.candle_1h_query.with_entities(
                    ep_table.timestamp, 
                    ep_table.close
                    ).filter(ep_table.timestamp > start_timestamp
                    ).order_by(ep_table.timestamp
                    ).first()
        if candle:
            return candle.close
        else:
            return 0

    def get_days_performance(self, days):
        try:
            end = self.ex_pair_close[0].close
            if end == 0:
                return 0
            if days == 1:
                start = self.ex_pair_close[0].close_day
            if days == 7:
                start = self.ex_pair_close[0].close_week
            if days == 30:
                start = self.ex_pair_close[0].close_month
        except:
            end = 1
            start = 1
        if self.base_currency.symbol == "BTC":
            start = 1 / start
            end = 1 / end
        elif self.quote_currency.symbol != "BTC":
            raise TypeError("ExPair does not include BTC as base or quote.")
        return float((end - start) / start) 

    def get_close(self):
        try:
            close = self.ex_pair_close[0].close
            if self.base_symbol == 'BTC':
                close = round(1 / close, 8)
            return close
        except:
            print('Unable to get close for ', self.base_symbol, '/', self.quote_symbol)
            return 0

    def get_last_timestamp(self):
        try:
            return self.candle_1h_last.timestamp
        except:
            return None

    def __repr__(self):
        return '{s.exchange.name} {s.base_symbol}/{s.quote_symbol} [{s.id}]'.format(s=self)

    __table_args__ = (UniqueConstraint('exchange_id', 'quote_currency_id', 'base_currency_id'), )


class ExPairClose(Base, Mixin):
    __tablename__ = 'ex_pair_close'

    ex_pair_id = Column(FKInteger, ForeignKey('ex_pairs.id'))
    close = Column(Numeric(24,12))
    close_day = Column(Numeric(24,12))
    close_week = Column(Numeric(24,12))
    close_month = Column(Numeric(24,12))


class ExPairLimits(Mixin, Base):
    __tablename__ = 'ex_pair_limits'

    ex_pair_id = Column(FKInteger, ForeignKey('ex_pairs.id'))
    min_amount = Column(Numeric(24,12))
    max_amount = Column(Numeric(24,12))
    min_value = Column(Numeric(24,12))
    max_value = Column(Numeric(24,12))


class ExternalAddress(Base, Mixin):
    __tablename__ = 'external_addresses'

    cube_id = Column(FKInteger, ForeignKey('cubes.id'))
    address = Column(String(50))
    managed_account = Column(Boolean, default=True)  # If true, CoinCube should consider this account when rebalancing
    address_type = Column(String(10)) 

    transactions = relationship('ExternalTransaction', backref='address', cascade='all, delete-orphan')

    @property
    def balances(self):
        sub_query = ExternalTransaction.query.with_entities(func.max(ExternalTransaction.id).label("id")).filter_by(address=self).group_by(ExternalTransaction.symbol).subquery('inner')
        return ExternalTransaction.query.join(sub_query, ExternalTransaction.id == sub_query.c.id).all()

    def __repr__(self):
        return '<ExternalAddress(id={s.id}, cube_id={s.cube_id}, address={s.address})>'.format(s=self)


class ExternalTransaction(Base, Mixin):
    __tablename__ = 'external_transactions'

    cube_id = Column(FKInteger, ForeignKey('cubes.id'))
    address_id = Column(FKInteger, ForeignKey('external_addresses.id'))
    symbol = Column(String(5))
    balance = Column(Numeric(24,12))

    @property
    def currency(self):
        return Currency.query.filter_by(symbol=self.symbol).first()

    def __repr__(self):
        return '<ExternalTransaction(id={s.id}, address={s.address.address})>'.format(s=self)


class FAQ(Base):
    __tablename__ = 'faqs'

    id = Column(FKInteger, primary_key=True)
    toc_id = Column(FKInteger, ForeignKey('faqs_toc.id'))
    sub_heading = Column(String(24))
    question = Column(String(255))
    answer = Column(Text)


class FAQTOC(Base):
    __tablename__ = 'faqs_toc'

    id = Column(FKInteger, primary_key=True)
    heading = Column(String(24))
    content = relationship('FAQ')


focus_currency_association_table = Table('focus_currencies', Base.metadata,
    Column('focus_id', FKInteger, ForeignKey('focuses.id')),
    Column('currency_id', FKInteger, ForeignKey('currencies.id'))
)


class Focus(Mixin, Base):
    __tablename__ = 'focuses'

    type = Column(String(255))
    count = Column(Numeric(10))

    currencies = relationship('Currency',
        secondary=focus_currency_association_table,
        order_by="Currency.market_cap")

    @property 
    def sorted_currencies(self):
        # Order by market capitalization (highest to lowest)
        ordered_assets = []
        for cur in self.currencies:
            ordered_assets.append((cur.market_cap, cur))
        ordered_assets.sort(key=lambda x: x[0] or 0, reverse=False)

        sorted_assets = []
        for x in ordered_assets:
            sorted_assets.append(x[1])

        return sorted_assets


class HourlyPerformance(Base):
    __tablename__ = 'hourly_performance'

    id = Column(FKInteger, primary_key=True)
    cube_id = Column(FKInteger, ForeignKey('cubes.id'))
    timestamp = Column(DateTime)
    btc_total = Column(Numeric(24,12))
    start_val = Column(Numeric(24,12))
    end_val = Column(Numeric(24,12))

    @property
    def daily_return(self):
        if self.start_val == 0:
            return 0
        else:
            return (self.end_val - self.start_val) / self.start_val

    __table_args__ = (UniqueConstraint('cube_id', 'timestamp'), )


class HourlyPerformanceAccount(Base):
    __tablename__ = 'hourly_performance_account'

    id = Column(FKInteger, primary_key=True)
    user_id = Column(FKInteger, ForeignKey('users.id'))
    timestamp = Column(DateTime)
    start_val = Column(Numeric(24,12))
    end_val = Column(Numeric(24,12))

    @property
    def daily_return(self):
        if self.start_val == 0:
            return 0
        else:
            return (self.end_val - self.start_val) / self.start_val

    __table_args__ = (UniqueConstraint('user_id', 'timestamp'), )


class IndexPair(Mixin, Base):
    __tablename__ = 'index_pairs'

    quote_currency_id = Column(FKInteger, ForeignKey('currencies.id'))
    base_currency_id = Column(FKInteger, ForeignKey('currencies.id'))
    quote_symbol = Column(String(10))
    base_symbol = Column(String(10))
    active = Column(Boolean)

    candle_1h = Column(Boolean, default=False)

    quote_currency = relationship('Currency',
        backref='index_pairs',
        foreign_keys=quote_currency_id)
    base_currency = relationship('Currency',
        foreign_keys=base_currency_id)
    index_pair_close = relationship('IndexPairClose')

    @property
    def candle_1h_query(self):
        return CombinedIndexPair1h.query.filter_by(
            quote_currency_id=self.quote_currency_id,
            base_currency_id=self.base_currency_id)

    @property 
    def candle_1h_data(self):
        return self.candle_1h_query.all()

    def get_close(self):
        try:
            return self.candle_1h_data[-1].close
        except:
            return 0

    def get_last_timestamp(self):
        try:
            return self.candle_1h_data[-1].timestamp
        except:
            return None

    def candle_1h_x_days(self, days):
        start_timestamp = datetime.utcnow() - timedelta(days=days)
        ip_table = CombinedIndexPair1h
        candle = self.candle_1h_query.with_entities(
                ip_table.timestamp, 
                ip_table.close
                ).filter(ip_table.timestamp > start_timestamp
                ).order_by(ip_table.timestamp
                ).first()
        if candle:
            return candle.close
        else:
            return 0

    def get_days_performance(self, days):
        try:
            end = self.index_pair_close[0].close
            if end == 0:
                return 0
            if days == 1:
                start = self.index_pair_close[0].close_day
            if days == 7:
                start = self.index_pair_close[0].close_week
            if days == 30:
                start = self.index_pair_close[0].close_month
            if start == 0:
                return 0
        except:
            end = 1
            start = 1
        if self.base_currency.symbol == "BTC":
            start = 1 / start
            end = 1 / end
        elif self.quote_currency.symbol != "BTC":
            raise TypeError("IndxPair does not include BTC as base or quote.")
        return float((end - start) / start) 

    def __repr__(self):
        return 'IndexPair {s.base_symbol}/{s.quote_symbol} [{s.id}]'.format(s=self)

    __table_args__ = (UniqueConstraint('quote_currency_id', 'base_currency_id'), )


class IndexPairClose(Base, Mixin):
    __tablename__ = 'index_pair_close'

    index_pair_id = Column(FKInteger, ForeignKey('index_pairs.id'))
    close = Column(Numeric(24,12))
    close_day = Column(Numeric(24,12))
    close_week = Column(Numeric(24,12))
    close_month = Column(Numeric(24,12))


class InstructionItem(Base):
    __tablename__ = 'instructions_items'

    id = Column(FKInteger, primary_key=True)
    instructions_id = Column(FKInteger, ForeignKey('instructions.id'))
    order = Column(Integer(2))
    item = Column(Text)


class Instruction(Base):
    __tablename__ = 'instructions'

    id = Column(FKInteger, primary_key=True)
    key = Column(String(20))
    title = Column(String(100))
    items = relationship('InstructionItem')


class Invoice(Mixin, Base):
    __tablename__ = 'invoices'
    user_id = Column(FKInteger, ForeignKey('users.id'))
    product_type = Column(String(10), ForeignKey('products.product_type'))
    amount = Column(Numeric(20, 8))
    currency = Column(Enum('USD', 'BTC'))
    address = Column(String(255), nullable=True)
    months = Column(Integer(3))
    paid_at = Column(DateTime, nullable=True)
    amount_paid = Column(Numeric(20, 8), nullable=True, default=0)
    product_price_id = Column(FKInteger, ForeignKey('product_prices.id'), default=None, nullable=True)
    user_product_id = Column(FKInteger, ForeignKey('user_products.id'), default=None, nullable=True)

    user = relationship('User')
    product = relationship('Product')
    product_price = relationship('ProductPrice')
    user_product = relationship('UserProduct')

    @property 
    def product_name(self):
        return self.product.full_name
        
    @property 
    def paid_at_timestamp(self):
        return str(self.paid_at.timestamp())


class Ledger(Mixin, Base):
    __tablename__ = 'ledger'

    user_id = Column(FKInteger, ForeignKey('users.id'))
    wallet_provider = Column(Enum('copay','gem','blockchain','stripe', 'coinpayments'))
    payment_address = Column(String(255), nullable=True)
    tx_hash = Column(String(255), nullable=True)
    txn_id = Column(Integer(10, unsigned=True), nullable=True)
    amount = Column(Numeric(24,12))
    fee = Column(Numeric(24,12), nullable=True)
    currency = Column(Enum('USD', 'BTC'))
    crypto_amount = Column(Numeric(24,12), nullable=True)
    crypto_currency = Column(String(50), nullable=True)
    type = Column(Enum('payment', 'credit', 'debit'))
    invoice_id = Column(FKInteger, ForeignKey('invoices.id'))

    invoice = relationship('Invoice')

    @property 
    def created_at_timestamp(self):
        return str(self.created_at.timestamp())

    def __repr__(self):
        return '<Ledger {s.id} (user_id={s.user_id} type={s.type})>'.format(s=self)


class Order(Mixin, Base):
    __tablename__ = 'open_orders'

    cube_id = Column(FKInteger, ForeignKey('cubes.id'))
    ex_pair_id = Column(FKInteger, ForeignKey('ex_pairs.id'))
    order_id = Column(String(255))
    side = Column(Enum('buy', 'sell'))
    price = Column(Numeric(24,12))
    amount = Column(Numeric(24,12))
    filled = Column(Numeric(24,12))
    unfilled = Column(Numeric(24,12))
    avg_price = Column(Numeric(24,12))
    pending = Column(Boolean, default=True)
    expires_at = Column(DateTime)

    ex_pair = relationship('ExPair')

    @property 
    def datetime(self):
        return self.created_at.strftime("%Y-%m-%d %H:%M:%S")

    @property 
    def timestamp(self):
        return str(self.created_at.timestamp())

    def __repr__(self):
        return '[%s] %s: %s %.8f@%.8f%s' % (self.order_id, self.ex_pair,
            self.side.capitalize(), self.amount, self.price, ' [P]' if self.pending else '')


class Point(Mixin, Base):
    __tablename__ = 'points'

    name = Column(String(128))
    action = Column(String(128))
    kind = Column(String(128))
    award = Column(String(255))
    value = Column(Integer(11))
    active = Column(Boolean)
    help_text = Column(String(255))

    def __repr__(self):
        return '<Point {s.name} (kind={s.kind} value={s.value})>'.format(s=self)

class Product(Base):
    __tablename__ = 'products'

    product_type = Column(String(10), primary_key=True)
    full_name = Column(String(100), nullable=False)
    description = Column(Text())
    enabled = Column(Boolean, default=True, nullable=False)
    id = Column(Integer(10))

    @property 
    def prices(self):
        product_prices = ProductPrice.query.filter_by(
                enabled=True,
                product_type=self.product_type,
            ).order_by(ProductPrice.months).all()
        prices = {}
        for pp in product_prices:
            prices[str(pp.months)] = pp.monthly_rate
        return prices

    @property 
    def features(self):
        pf = ProductFeature.query.filter_by(
                    product_type=self.product_type,
                    ).first()
        return [pf.f_one, pf.f_two, pf.f_three, pf.f_four]


class ProductFeature(Mixin, Base):
    __tablename__ = 'product_features'

    product_type = Column(String(10), ForeignKey('products.product_type'))
    f_one = Column(String(256))
    f_two = Column(String(256))
    f_three = Column(String(256))
    f_four = Column(String(256))

    __table_args__ = (UniqueConstraint('product_type'), )


class ProductPrice(Mixin, Base):
    __tablename__ = 'product_prices'

    product_type = Column(String(10), ForeignKey('products.product_type'))
    months = Column(Integer(3), nullable=False)
    monthly_rate = Column(Numeric(6, 2), nullable=False)
    enabled = Column(Boolean, default=True, nullable=False)

    product = relationship('Product')

    __table_args__ = (UniqueConstraint('months', 'product_type'), )


class RevokedToken(Base):
    __tablename__ = 'revoked_tokens'
    id = Column(Integer, primary_key=True)
    jti = Column(String(120))
    
    def add(self):
        db_session.add(self)
        db_session.commit()
    
    @classmethod
    def is_jti_blacklisted(cls, jti):
        query = cls.query.filter_by(jti = jti).first()
        return bool(query)


class Reward(Mixin, Base):
    __tablename__ = 'rewards'

    name = Column(String(128))
    action = Column(String(128))
    kind = Column(String(128))
    award = Column(String(255))
    value = Column(Integer(11))
    active = Column(Boolean)
    discount = Column(Numeric(20,8))
    duration = Column(Integer(11))
    help_text = Column(String(255))

    def __repr__(self):
        return '<Reward {s.name} (kind={s.kind} value={s.value})>'.format(s=self)


# Define the Role data model
class Role(Mixin, Base):
    __tablename__ = 'roles'

    name = Column(String(50), unique=True)

    def __repr__(self):
        return '<Role {s.name} (id={s.id})>'.format(s=self)


# Define the RoleUsers data model
class RoleUsers(Mixin, Base):
    __tablename__ = 'role_users'

    user_id = Column(FKInteger, ForeignKey('users.id', ondelete='CASCADE'))
    role_id = Column(FKInteger, ForeignKey('roles.id', ondelete='CASCADE'))

    def __repr__(self):
        return '<RoleUsers {s.role_id} (id={s.id} user_id={s.user_id})>'.format(s=self)


class Team(Base):
    __tablename__ = 'team_members'

    id = Column(FKInteger, primary_key=True)
    name = Column(String(24))
    title = Column(String(24))
    biography = Column(Text)
    github_url = Column(String(255))
    linkedin_url = Column(String(255))
    twitter_url = Column(String(255))
    image_location = Column(String(255))

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class TransactionFull(Base):
    __tablename__ = 'transactions_full'

    id = Column(FKInteger, primary_key=True)
    datetime = Column(DateTime)
    user_id = Column(FKInteger, ForeignKey('users.id'))
    cube_id = Column(FKInteger, ForeignKey('cubes.id'))
    exchange_id = Column(FKInteger, ForeignKey('exchanges.id'))
    base_symbol = Column(String(10))
    quote_symbol = Column(String(10))
    tx_id = Column(String(255))
    order_id = Column(String(255), default=None)
    address = Column(String(255), default=None)
    tag = Column(String(255), default=None)
    type = Column(Enum('buy', 'sell', 'deposit', 'withdrawal'))
    trade_type = Column(Enum('limit', 'market'), default=None)
    price = Column(Numeric(24,12), default=None)
    fee_currency = Column(String(10))
    fee_amount = Column(Numeric(24,12))
    fee_rate = Column(Numeric(14,12))
    ignore = Column(Boolean, default=False)
    airdrop = Column(Boolean, default=False)
    fork = Column(Boolean, default=False)
    payment = Column(Boolean, default=False)
    mined = Column(Boolean, default=False)
    transfer = Column(Boolean, default=False)
    needs_review = Column(Boolean, default=False)
    quote_amount = Column(Numeric(24,12))
    base_amount = Column(Numeric(24,12))

    user = relationship('User')
    cube = relationship('Cube')
    exchange = relationship('Exchange')

    def __repr__(self):
        return '[%s] %s/%s: %s [%s]' % (self.id, self.base_symbol,
            self.quote_symbol, self.type.capitalize(), self.datetime)

    @property
    def timestamp(self):
        return str(self.datetime.timestamp() * 1000)


class Transaction(Mixin, Base):
    __tablename__ = 'transactions'

    user_id = Column(FKInteger, ForeignKey('users.id'))
    cube_id = Column(FKInteger, ForeignKey('cubes.id'))
    ex_pair_id = Column(FKInteger, ForeignKey('ex_pairs.id'))
    type = Column(Enum('buy', 'sell', 'withdraw', 'deposit', 'convert', 'failure'))

    # properties defined later for automatically setting amounts from balances
    _quote_balance = Column('quote_balance', Numeric(24,12))
    _base_balance = Column('base_balance', Numeric(24,12))
    _quote_amount = Column('quote_amount', Numeric(24,12))
    _base_amount = Column('base_amount', Numeric(24,12))

    api_response = Column(String(255))
    exchange_rate = Column(Numeric(24,12))
    user_notified = Column(Boolean, default=False)

    user = relationship('User')
    ex_pair = relationship('ExPair')

    def __init__(self, *args, **kwargs):
        # TODO: not ideal but this allows user_id to be set to the cube's user_id
        super(Transaction, self).__init__(*args, **kwargs)
        try:
            self.user = self.cube.user
        except:
            pass

    def __repr__(self):
        return '[%s] %s: %s [%s]' % (self.id, self.ex_pair,
            self.type.capitalize(), self.created_at)

    @property 
    def datetime(self):
        return self.created_at.strftime("%Y-%m-%d %H:%M:%S")

    @property 
    def timestamp(self):
        return str(self.created_at.timestamp())

    @hybrid_property
    def quote_balance(self):
        return self._quote_balance

    @quote_balance.expression
    def quote_balance(cls):
        return cls._quote_balance

    @quote_balance.setter
    def quote_balance(self, value):
        self._quote_balance = value
        try:
            self._quote_amount = self._calc_amount(self.ex_pair.quote_currency_id)
        except AttributeError:
            pass

    @hybrid_property
    def base_balance(self):
        return self._base_balance

    @base_balance.expression
    def base_balance(cls):
        return cls._base_balance

    @base_balance.setter
    def base_balance(self, value):
        self._base_balance = value
        try:
            self._base_amount = self._calc_amount(self.ex_pair.base_currency_id)
        except AttributeError:
            pass

    def _calc_amount(self, cur_id):
        if cur_id not in [
            self.ex_pair.quote_currency_id,
            self.ex_pair.base_currency_id]:
            # invalid cur_id...
            raise ValueError('Invalid cur_id')
        ex_id = self.ex_pair.exchange_id
        last = self.cube.transactions

        if self.id:
            last = last.filter(Transaction.id < self.id)

        last = last.filter(
            # Transaction.id != self.id,
            Transaction.ex_pair.has(and_(
                ExPair.exchange_id == ex_id,
                or_(
                    ExPair.quote_currency_id == cur_id,
                    ExPair.base_currency_id == cur_id
                )))
            ).order_by(Transaction.id.desc()).first()

        if cur_id == self.ex_pair.quote_currency_id:
            if not last:
                # first occurence of "ex_cur"
                bal = float(self.quote_balance)
            elif cur_id == last.ex_pair.quote_currency_id:
                bal = float(self.quote_balance) - float(last.quote_balance)

            else:  # last.ex_pair.base_currency_id
                bal = float(self.quote_balance) - float(last.base_balance)
            return bal

        else:  # self.ex_pair.base_currency_id
            if not last:
                bal = float(self.base_balance)
            elif cur_id == last.ex_pair.base_currency_id:
                bal = float(self.base_balance) - float(last.base_balance)
            else:
                bal = float(self.base_balance) - float(last.quote_balance)
            return bal

    @property
    def quote_amount(self):
        if self._quote_amount is None:
            self._quote_amount = self._calc_amount(self.ex_pair.quote_currency_id)
        return self._quote_amount

    @quote_amount.setter
    def quote_amount(self, value):
        self._quote_amount = value

    @property
    def base_amount(self):
        if self._base_amount is None:
            self._base_amount = self._calc_amount(self.ex_pair.base_currency_id)
        return self._base_amount

    @base_amount.setter
    def base_amount(self, value):
        self._base_amount = value


class User(Mixin, Base, UserMixin):
    __tablename__ = 'users'

    social_id = Column(String(128))
    email = Column(String(128), nullable=True, unique=True)
    password_hash = Column(String(192), nullable=True)
    first_name = Column(String(128), nullable=False)
    agreement = Column(String(128), nullable=False)
    last_login = Column(DateTime, default=datetime.utcnow)
    news = Column(Boolean, default=True)
    alerts = Column(Boolean, default=True)
    otp_secret = Column(String(16))
    otp_complete = Column(Boolean, default=False)
    cb_wallet_id = Column(String(128))
    cb_refresh_token = Column(String(255))
    portfolio = Column(Boolean, default=False)
    new_portfolio = Column(Boolean, default=False)
    email_confirmed = Column(Boolean, default=False)
    wide_charts = Column(Boolean, default=False)
    fiat_id = Column(FKInteger, ForeignKey('currencies.id'))
    btc_data = Column(Boolean, default=False, nullable=True)

    ledger_entries =  relationship('Ledger',
        lazy='dynamic',
        backref='user')
    api_keys =  relationship('UserApiKey',
        lazy='dynamic',
        backref='user')
    notifications = relationship('UserNotification',
        lazy='dynamic',
        backref='user')

    # Relationships
    roles = relationship('Role',
        secondary='role_users',
        backref='users',
        lazy='dynamic')

    cubes =  relationship('Cube')
    fiat = relationship('Currency')
    daily_performance = relationship('DailyPerformanceAccount',
        lazy='dynamic',
        backref='user')
    hourly_performance = relationship('HourlyPerformanceAccount',
        lazy='dynamic',
        backref='user')

    @property 
    def role(self):
        role = self.roles.first()
        if role:
            return role.name
        else:
            return 'Registered'

    @property 
    def available_algorithms(self):
        return Algorithm.query.filter_by(active=True).all()

    @property 
    def available_exchanges(self):
        # Exclude 12 (External)
        taken_exchanges = [12]
        for cube in self.cubes:
            for conn in cube.api_connections:
                taken_exchanges.append(conn.exchange.id)

        exchanges = Exchange.query.filter_by(active=True).filter(
                                ~Exchange.id.in_(taken_exchanges)
                                ).all()
        return exchanges

    @property 
    def open_cubes(self):
        # Return all open Cubes which are not External/Manual
        return Cube.query.join(Connection).filter_by(
                            user_id=self.id
                            ).filter(and_(
                            Connection.exchange_id != 12,
                            Connection.exchange_id != 13
                            )).all()

    @property 
    def open_wallets(self):
        # Return all open Cubes which are External/Manual
        return Cube.query.filter_by(
                            user_id=self.id
                            ).filter(or_(
                            Cube.exchange_id == 12,
                            Cube.exchange_id == 13
                            )).all()

    @property
    def total_payments(self):
        total = 0
        for l in self.ledger_entries:
            total += l.amount
        return total

    @property
    def password(self):
        raise AttributeError('Password is not a readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def set_totp_secret(self):
        self.otp_secret = base64.b32encode(os.urandom(10)).decode('utf-8')

    def get_totp_uri(self):
        # set_totp_secret()
        return 'otpauth://totp/COINCUBE:{0}?secret={1}&issuer=COINCUBE' \
            .format(self.email, self.otp_secret)

    def verify_totp(self, token):
        return otp.valid_totp(token, self.otp_secret)

    @property
    def is_pro(self):
        return UserProduct.query.filter(
            UserProduct.user_id == self.id,
            UserProduct.product_type == 'pro',
             UserProduct.paid_until >= datetime.utcnow()
        ).count() > 0

    @property
    def is_basic(self):
        return UserProduct.query.filter(
            UserProduct.user_id == self.id,
            UserProduct.product_type == 'basic',
            UserProduct.paid_until >= datetime.utcnow()
        ).count() > 0

    def data_frame(self, query, columns):
        # Takes a sqlalchemy query and a list of columns, returns a dataframe.
        def make_row(x):
            return dict([(c, getattr(x, c)) for c in columns])
        return pd.DataFrame([make_row(x) for x in query])

    def get_daily_performance(self, start_date=None, end_date=None):
        try:
            if start_date and end_date:
                query = self.daily_performance.filter(
                                DailyPerformanceAccount.timestamp >= start_date,
                                DailyPerformanceAccount.timestamp <= end_date
                                )
            elif start_date:
                query = self.daily_performance.filter(
                                DailyPerformanceAccount.timestamp >= start_date)   
            elif end_date:
                query = self.daily_performance.filter(
                                DailyPerformanceAccount.timestamp <= end_date)                   
            else:
                query = self.daily_performance
            dp = pd.read_sql(query.statement, query.session.bind)
            dp = dp.set_index('timestamp')
            return dp
        except:
            return pd.DataFrame()

    def get_hourly_performance(self, start_date=None, end_date=None):
        try:
            if start_date and end_date:
                query = self.hourly_performance.filter(
                                HourlyPerformanceAccount.timestamp >= start_date,
                                HourlyPerformanceAccount.timestamp <= end_date
                                )
            elif start_date:
                query = self.hourly_performance.filter(
                                HourlyPerformanceAccount.timestamp >= start_date)   
            elif end_date:
                query = self.hourly_performance.filter(
                                HourlyPerformanceAccount.timestamp <= end_date)                   
            else:
                query = self.hourly_performance
            dp = pd.read_sql(query.statement, query.session.bind)
            dp = dp.set_index('timestamp')
            return dp
        except:
            return pd.DataFrame()

    def calc_daily_performance(self, start_date=None):
        df = pd.DataFrame()

        if self.cubes:
            for cube in self.cubes:
                if not cube.closed_at:
                    if df.empty:
                        dp = cube.get_daily_performance(start_date=start_date)
                        if not dp.empty:
                            df = dp[['start_val', 'end_val']]
                    else:
                        dp = cube.get_daily_performance(start_date=start_date)
                        if not dp.empty:
                            df = df.add(dp[['start_val', 'end_val']], fill_value=0)

            df['user_id'] = self.id
            return df

    def calc_hourly_performance(self, start_date=None):
        df = pd.DataFrame()

        if self.cubes:
            for cube in self.cubes:
                if not cube.closed_at:
                    if df.empty:
                        dp = cube.get_hourly_performance(start_date=start_date)
                        if not dp.empty:
                            df = dp[['start_val', 'end_val']]
                    else:
                        dp = cube.get_hourly_performance(start_date=start_date)
                        if not dp.empty:
                            df = df.add(dp[['start_val', 'end_val']], fill_value=0)

            df['user_id'] = self.id
            return df

    def daily_performance_gen(self):
        log.debug('[self %d] Start DP' % self.id)

        last = self.daily_performance.order_by(
                            DailyPerformanceAccount.timestamp.desc()
                            ).first()
        if last:
            start_date = pd.to_datetime(last.timestamp.date())
        else:
            start_date = None

        df = self.calc_daily_performance(start_date)
        if df is None:
            log.debug('[User %d] No data' % self.id)
            return None

        if last:
            db_session.delete(last)
            db_session.commit()

        df.to_sql(DailyPerformanceAccount.__table__.name, 
                              db_session.bind, 
                              index=True, 
                              if_exists='append')

    def hourly_performance_gen(self):
        log.debug('[self %d] Start DP' % self.id)

        last = self.hourly_performance.order_by(
                            HourlyPerformanceAccount.timestamp.desc()
                            ).first()
        if last:
            start_date = pd.to_datetime(last.timestamp.date())
        else:
            start_date = None

        df = self.calc_hourly_performance(start_date)
        if df is None:
            log.debug('[User %d] No data' % self.id)
            return None

        if last:
            db_session.delete(last)
            db_session.commit()

        df.to_sql(HourlyPerformanceAccount.__table__.name, 
                              db_session.bind, 
                              index=True, 
                              if_exists='append')

    def get_performance(self, start_date=None, end_date=None, timeframe='hourly'):
        btc = Currency.query.filter_by(symbol='BTC').one()

        if timeframe == 'daily':
            # Import data from 
            dp = self.get_daily_performance(start_date, end_date)
        if timeframe == 'hourly':
            dp = self.get_hourly_performance(start_date, end_date)
        # Fiat total and percent return
        index_pair = IndexPair.query.filter_by(
            quote_currency_id=self.fiat.id,
            base_currency_id=btc.id
            ).first()
        query = index_pair.candle_1h_query.filter(
            func.time(CombinedIndexPair1h.timestamp) == time(23),
            CombinedIndexPair1h.timestamp >= (dp.index[0] + pd.Timedelta('23h')),
            CombinedIndexPair1h.timestamp <= (dp.index[-1] + pd.Timedelta('23h'))
            ).all()
        # get latest candle for end day
        last = index_pair.candle_1h_query.filter(
            CombinedIndexPair1h.timestamp <= (dp.index[-1] + pd.Timedelta('23h'))
            ).order_by(CombinedIndexPair1h.timestamp.desc()).first()
        if last:
            query.append(last)

        # Multiply btc total by fiat.id currency for fiat total
        df2 = self.data_frame(query, ['timestamp', 'close'])
        df2 = df2.set_index('timestamp')
        df2 = df2.resample('24H').first()
        df2 = df2.reindex(pd.date_range(dp.index[0], dp.index[-1]))
        df2 = df2.ffill()
        dp['btc_total'] = dp.end_val
        dp['fiat_total'] = dp.btc_total * df2.close.astype('float64')

        dp['fiat_val_start'] = dp.start_val * df2.close.astype('float64').shift().fillna(df2.close.astype('float64'))
        dp['fiat_val_end'] = dp.end_val * df2.close.astype('float64')

        # Fiat Percentage returns
        dp['fiat_pct_daily'] = (dp.fiat_val_end - dp.fiat_val_start) / dp.fiat_val_start
        dp['fiat_pct'] = dp.fiat_pct_daily.cumsum()
        dp.fiat_pct = dp.fiat_pct * 100

        dp['btc_pct_daily'] = (dp.end_val - dp.start_val) / dp.start_val
        dp['btc_pct'] = dp.btc_pct_daily.cumsum()
        dp.btc_pct = dp.btc_pct * 100

        return dp

    def __repr__(self):
        return '<User {s.id} ({s.email})>'.format(s=self)


class UserNotification(Mixin, Base):
    __tablename__ = 'user_notifications'
    user_id = Column(FKInteger, ForeignKey('users.id'))
    entity_id = Column(BigInteger)
    type = Column(String(255))
    message = Column(String(255))

    def __repr__(self):
        return '<UserNotification {s.id} (user_id={s.user_id} type={s.type})>'.format(s=self)


class UserApiKey(Mixin, Base):
    __tablename__ = 'user_api_keys'
    user_id = Column(FKInteger, ForeignKey('users.id'))
    key = Column(String(60))
    secret = Column(String(60))

    def __repr__(self):
        return '<UserApiKey {s.id} (user_id={s.user_id} key={s.key})>'.format(s=self)


class UserAgreement(Mixin, Base):
    __tablename__ = 'user_agreements'
    version = Column(Numeric(20,2))
    location = Column(String(255))

    def __repr__(self):
        return '<UserAgreement {s.id} (version={s.version} location={s.location})>'.format(s=self)


class UsersPerExchange(Base):
    __tablename__ = 'users_per_ex'

    index = Column(DateTime, primary_key=True)
    Bitfinex = Column(Numeric(24,12))
    Bitstamp = Column(Numeric(24,12))
    Bittrex = Column(Numeric(24,12))
    Coinbase = Column(Numeric(24,12))
    Gemini = Column(Numeric(24,12))
    Kraken = Column(Numeric(24,12))
    Poloniex = Column(Numeric(24,12))
    HitBTC = Column(Numeric(24,12))
    Binance = Column(Numeric(24,12))
    total = Column(Numeric(24,12))

    def __repr__(self):
        return '<UsersPerExchange(id={s.index}, total={s.total})>'.format(s=self)

class UserPoint(Mixin, Base):
    __tablename__ = 'user_points'

    user_id = Column(FKInteger, ForeignKey('users.id'))
    point_id = Column(FKInteger, ForeignKey('points.id'))

    point = relationship('Point',
        foreign_keys=point_id)

    def __repr__(self):
        return '<UserPoint {s.user_id} (point_id={s.point_id})>'.format(s=self)

class UserProduct(Mixin, Base):
    __tablename__ = 'user_products'

    user_id = Column(FKInteger, ForeignKey('users.id'))
    product_type = Column(String(10), ForeignKey('products.product_type'))
    paid_until = Column(DateTime)
    subscribed = Column(Boolean, default=False, nullable=False)
    stripe_id = Column(String(24))

    user = relationship('User')
    product = relationship('Product')

    __table_args__ = (UniqueConstraint('user_id', 'product_type'), )

    @property 
    def paid_until_timestamp(self):
        return str(self.paid_until.timestamp())

    def get_last_invoice(self):
        return Invoice.query.filter_by(
                user_product_id=self.id
            ).order_by(Invoice.created_at.desc()).first()

    def get_invoices(self):
        return Invoice.query.filter_by(
                user_product_id=self.id
            ).order_by(Invoice.created_at.desc()).all()


class UserReward(Mixin, Base):
    __tablename__ = 'user_rewards'

    user_id = Column(FKInteger, ForeignKey('users.id'))
    reward_id = Column(FKInteger, ForeignKey('rewards.id'))

    reward = relationship('Reward',
        foreign_keys=reward_id)

    def __repr__(self):
        return '<UserReward {s.user_id} (reward_id={s.reward_id})>'.format(s=self)

