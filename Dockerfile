FROM continuumio/anaconda3:5.0.1
RUN apt-get update && apt-get install -y build-essential mysql-client redis-tools

RUN conda install -c bioconda mysqlclient
RUN conda install -c conda-forge passlib flask-login flask-wtf flask-mail celery requests
RUN pip install --upgrade pip
RUN pip install flask-bcrypt flask-recaptcha

RUN pip install coinbase blockcypher onetimepass pyqrcode rauth pypng mailchimp3

RUN pip install --upgrade pip

COPY requirements.txt /database/requirements.txt
RUN pip install -r /database/requirements.txt

ADD . /database

WORKDIR /database
