"""add columns to currency table 1

Revision ID: 90a62273bf4a
Revises: 6688aa1d1f3c
Create Date: 2018-11-13 18:57:09.145574

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '90a62273bf4a'
down_revision = '6688aa1d1f3c'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
