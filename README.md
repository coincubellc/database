<h1>Database Submodule</h1>

<h3>Git Submodule Tutorial <a href="https://git-scm.com/book/en/v2/Git-Tools-Submodules">#1</a></h3>
<h3>Git Submodule Tutorial <a href="https://git.wiki.kernel.org/index.php/GitSubmoduleTutorial">#2</a></h3>

<h3>To run migration</h3>
`$ python manage.py db init`<br>
`$ python manage.py db migrate`<br>
`$ python manage.py db upgrade`<br>
`$ python manage.py db --help`<br>